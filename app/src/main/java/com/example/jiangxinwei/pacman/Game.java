package com.example.jiangxinwei.pacman;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import java.text.DecimalFormat;
import java.util.ArrayList;

class Game {
    private Walls wallsHorizon;
    private Walls wallsVertic;
    private Beans beans;
    private Chasers chasers;
    private Computer computer;
    private boolean computerHitByChaser = false;
    private boolean beansEmpty = false;   //when there is no beans, then game finished
    private String computerScore = "0";
    private boolean removed;

    public static final float MAXXY = 1.0f;
    public static final float MINXY = 0.0f;
    public static final int SCOREINCREASE = 10;  //eat one bean, score increases 10


    public Game()
    {
        wallsHorizon = Walls.createWallsHorizon();
        wallsVertic = Walls.createWallsVertic();
        beans = Beans.createBeans();
        chasers = new Chasers();
        computer =  new Computer();
    }

    public void draw(Canvas canvas, Paint paint) {
        int h = canvas.getHeight();
        int w = canvas.getWidth();
        paint.setTextSize(50.0f);
        canvas.drawText(computerScore, 0.07f*w, 0.2f*h, paint);
        wallsHorizon.drawH(canvas, paint);
        wallsVertic.drawV(canvas, paint);
        beans.draw(canvas, paint);
        chasers.draw(canvas, paint);
        if(!computerHitByChaser) {
            computer.draw(canvas, paint);
        }
    }

    public void step(){
        chasers.step();

        if(!computerHitByChaser) {   //computer can move if it has not eaten by chaser
            //Find the closest bean
            Float smallestSteps = 50f;
            Float steps;
            Float closeX = 0f;
            Float closeY = 0f;
            boolean accepted;
            //Log.d("computer position", String.valueOf(computer.pos.x) + " " + String.valueOf(computer.pos.y));
            for (Bean b : beans) {
                steps = computer.pos.stepCount(b.pos);
                //Log.d("close", String.valueOf(b.pos.x) + " " + String.valueOf(b.pos.y));
                accepted = b.noWall(computer, wallsHorizon, wallsVertic);
                //Log.d("dis", String.valueOf(steps) + " " + String.valueOf(smallestSteps));
                if (steps < smallestSteps && accepted) {
                    smallestSteps = steps;
                    closeX = b.pos.x;
                    closeY = b.pos.y;
                }
            }
            //Log.d("final close", String.valueOf(closeX) + " " + String.valueOf(closeY));

            //store the valid move for computer to avoid chaser
            ArrayList<String> moveAvoidChaser = computer.avoidChaser(chasers);
            ArrayList<String> moveAvoidWall = computer.avoidWall(wallsHorizon, wallsVertic);

            //Let computer move;
            computer.step(closeX, closeY, moveAvoidChaser, moveAvoidWall);

            //Remove bean which has been eaten
            removed = beans.removeEat(computer);
            if(removed == true){
                computerScore = String.valueOf(Integer.parseInt(computerScore) + SCOREINCREASE);
            }

            //check if computer is hit by chasers
            computerHitByChaser = computer.hitByChaser(chasers);
        }
    }
}
